source 'https://rubygems.org'

gem 'rails', '4.2.3'
gem 'pg'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'bcrypt'
gem "rack-cors", :require => "rack/cors"


group :development, :test do
  gem 'byebug'
  gem 'web-console', '~> 2.0'
  gem 'spring'
end

# Custom gems

# Development
gem 'versionist'
gem 'puma'
gem 'cancancan'
gem 'rolify'
gem 'paper_trail', '~> 4.0.0.rc'
gem 'paranoia', '~> 2.0'
gem 'colorize'
gem 'browser'
gem 'kaminari'
gem 'awesome_print'
gem 'country_select'
gem 'active_model_serializers'
gem 'friendly_id', '~> 5.1.0'
gem 'conekta'

group :development do
  gem 'spring-commands-rspec'
  gem 'letter_opener'
end

group :development, :test do
  gem 'factory_girl_rails'
  gem 'pry-rails'
  gem 'rspec-rails', '~> 3.3.0'
  gem 'rspec-collection_matchers'
  gem 'ffaker'
  gem 'rubocop', require: false
  gem 'simplecov', require: false
end

# Test gems
group :test do
  gem 'database_cleaner'
  gem 'shoulda-matchers'
end

# Production and staging gems
group :production, :staging do
  gem 'rails_12factor'
  gem 'exception_notification'
end
