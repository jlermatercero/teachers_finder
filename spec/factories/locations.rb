# == Schema Information
#
# Table name: locations
#
#  id           :integer          not null, primary key
#  street       :string
#  number       :string
#  neighborhood :string
#  city         :string
#  state        :string
#  zip_code     :integer
#  country      :string
#  lat          :string
#  lon          :string
#  calendar_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

FactoryGirl.define do
  factory :location do
    street { FFaker::Address.street_name }
    number { FFaker::Address.building_number }
    neighborhood { FFaker::Address.neighborhood }
    city { FFaker::Address.city }
    zip_code { FFaker::AddressUS.zip_code }
    state { FFaker::AddressUS.state }
    country { FFaker::Address.country }
    calendar
  end
end
