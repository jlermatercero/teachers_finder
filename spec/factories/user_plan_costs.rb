# == Schema Information
#
# Table name: user_plan_costs
#
#  id         :integer          not null, primary key
#  plan_id    :integer
#  user_id    :integer
#  cost       :decimal(8, 2)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :user_plan_cost do
    plan
    user
    cost { (1000.0 - 5.0) * rand + 5 } # to generate random decimal
  end
end
