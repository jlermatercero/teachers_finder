# == Schema Information
#
# Table name: calendars
#
#  id         :integer          not null, primary key
#  teacher_id :integer
#  user_id    :integer
#  date_start :datetime
#  date_end   :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :calendar do
    teacher { create :user }
    user
    date_start { FFaker::Time.date }
    date_end { FFaker::Time.date }
  end
end
