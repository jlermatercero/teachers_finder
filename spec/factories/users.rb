# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string
#  password_digest :string
#  auth_token      :string
#  first_name      :string
#  last_name       :string
#  avatar          :string
#  votes_count     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :user do
    email { FFaker::Internet.email }
    password '12345'
    password_confirmation '12345'
    first_name { FFaker::Name.first_name }
    last_name { FFaker::Name.last_name }
  end
end
