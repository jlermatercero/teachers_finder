# == Schema Information
#
# Table name: user_skills
#
#  id         :integer          not null, primary key
#  skill_id   :integer
#  user_id    :integer
#  experience :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :user_skill do
    skill
    user
    experience { rand(100) }
  end
end
