require 'rails_helper'

RSpec.describe Api::V1::VotesController, type: :controller do
  let(:user) { create :user }

  context 'when user is not logged in' do
    it 'returns error unauthorized' do
      get :index
      expect(response.response_code).to eq 401
    end
  end

  context 'when user logged in' do
    before do
      authorization_header(user.auth_token, user.email)
    end

    describe 'GET #index' do
      before do
        4.times { create :vote }
        get :index
      end

      it 'renders a list of votes_mades' do
        expect(json_response[:votes]).to have(4).items
      end

      it { should respond_with 200 }

      it_behaves_like 'paginated list'
    end

    describe 'POST #create' do
      context 'with valid params' do
        before do
          teacher = create :user
          vote = {
            user_id: teacher.id
          }
          post :create, vote: vote
        end

        it 'renders a votes_made record' do
          expect(json_response[:vote]).to have_key(:id)
        end

        it { should respond_with 201 }
      end

      context 'with invalid params' do
        before do
          vote = { user_id: '' }
          post :create, vote: vote
        end

        it 'returns errors if votes_made not valid' do
          expect(json_response[:errors][:user_id]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'DELETE #destroy' do
      before do
        vote = create :vote, author: user
        delete :destroy, id: vote.id
      end

      it { should respond_with 204 }
    end
  end
end
