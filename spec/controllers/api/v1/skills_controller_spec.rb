require 'rails_helper'

RSpec.describe Api::V1::SkillsController, type: :controller do
  let(:user) { create :user }

  context 'when user is not logged in' do
    it 'returns error unauthorized' do
      get :index
      expect(response.response_code).to eq 401
    end
  end

  context 'when user logged in' do
    before do
      authorization_header(user.auth_token, user.email)
    end

    describe 'GET #index' do
      before do
        4.times { create :skill }
        get :index
      end

      it 'renders a list of skills' do
        expect(json_response[:skills]).to have(4).items
      end

      it { should respond_with 200 }

      it_behaves_like 'paginated list'
    end

    describe 'POST #create' do
      context 'with valid params' do
        before do
          category = create :category
          skill = {
            name: 'my skill', category_id: category.id
          }
          post :create, skill: skill
        end

        it 'renders a skill record' do
          expect(json_response[:skill]).to have_key(:id)
        end

        it { should respond_with 201 }
      end

      context 'with invalid params' do
        before do
          skill = {
            name: ''
          }
          post :create, skill: skill
        end

        it 'returns errors if skill not valid' do
          expect(json_response[:errors][:name]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'GET #show' do
      before do
        @skill = create :skill
        get :show, id: @skill.id
      end

      it 'renders a single record' do
        expect(json_response[:skill][:id]).to eq @skill.id
      end

      it { should respond_with 200 }
    end

    describe 'PUT/PATCH #update' do
      context 'with valid params' do
        before do
          skill = create :skill
          put :update, id: skill.id, skill: { name: 'changed' }
        end

        it 'updates the skill name' do
          expect(json_response[:skill][:name]).to eq 'changed'
        end

        it { should respond_with 200 }
      end

      context 'with invalid params' do
        before do
          skill = create :skill
          put :update, id: skill.id, skill: { name: '' }
        end

        it 'returns error cant be blank' do
          expect(json_response[:errors][:name]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'DELETE #destroy' do
      before do
        skill = create :skill
        delete :destroy, id: skill.id
      end

      it { should respond_with 204 }
    end
  end
end
