require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  let(:user) { create :user }

  context 'when user is not logged in' do
    it 'returns error unauthorized' do
      get :index
      expect(response.response_code).to eq 401
    end
  end

  context 'when user logged in' do
    before do
      authorization_header(user.auth_token, user.email)
    end

    describe 'GET #index' do
      before do
        3.times { create :user }
        get :index
      end

      it 'renders a list of users' do
        expect(json_response[:users]).to have(4).items
      end

      it { should respond_with 200 }

      it_behaves_like 'paginated list'
    end

    describe 'POST #create' do
      context 'with valid params' do
        before do
          user = {
            email: FFaker::Internet.email, password: '123',
            password_confirmation: '123', first_name: 'first_name',
            last_name: 'last_name'
          }
          post :create, user: user
        end

        it 'renders a user record' do
          expect(json_response[:user]).to have_key(:id)
        end

        it { should respond_with 201 }
      end

      context 'with invalid params' do
        before do
          user = {
            email: '', password: '', password_confirmation: '', first_name: '',
            last_name: ''
          }
          post :create, user: user
        end

        it 'returns errors if user not valid' do
          expect(json_response[:errors][:email]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'GET #show' do
      before do
        @user = create :user
        get :show, id: @user.id
      end

      it 'renders a single record' do
        expect(json_response[:user][:id]).to eq @user.id
      end

      it { should respond_with 200 }
    end

    describe 'PUT/PATCH #update' do
      context 'with valid params' do
        before do
          user = create :user
          put :update, id: user.id, user: { first_name: 'changed' }
        end

        it 'updates the user experience' do
          expect(json_response[:user][:first_name]).to eq 'changed'
        end

        it { should respond_with 200 }
      end

      context 'with invalid params' do
        before do
          user = create :user
          put :update, id: user.id, user: { first_name: '' }
        end

        it 'returns error cant be blank' do
          expect(json_response[:errors][:first_name]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'DELETE #destroy' do
      before do
        user = create :user
        delete :destroy, id: user.id
      end

      it { should respond_with 204 }
    end
  end
end
