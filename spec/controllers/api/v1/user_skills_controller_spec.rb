require 'rails_helper'

RSpec.describe Api::V1::UserSkillsController, type: :controller do
  let(:user) { create :user }

  context 'when user is not logged in' do
    it 'returns error unauthorized' do
      get :index
      expect(response.response_code).to eq 401
    end
  end

  context 'when user logged in' do
    before do
      authorization_header(user.auth_token, user.email)
    end

    describe 'GET #index' do
      before do
        4.times { create :user_skill }
        get :index
      end

      it 'renders a list of user_skills' do
        expect(json_response[:user_skills]).to have(4).items
      end

      it { should respond_with 200 }

      it_behaves_like 'paginated list'
    end

    describe 'POST #create' do
      context 'with valid params' do
        before do
          skill = create :skill
          user_skill = {
            experience: 'my user_skill', skill_id: skill.id
          }
          post :create, user_skill: user_skill
        end

        it 'renders a user_skill record' do
          expect(json_response[:user_skill]).to have_key(:id)
        end

        it { should respond_with 201 }
      end

      context 'with invalid params' do
        before do
          user_skill = {
            experience: ''
          }
          post :create, user_skill: user_skill
        end

        it 'returns errors if user_skill not valid' do
          expect(json_response[:errors][:experience]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'GET #show' do
      before do
        @user_skill = create :user_skill, user: user
        get :show, id: @user_skill.id
      end

      it 'renders a single record' do
        expect(json_response[:user_skill][:id]).to eq @user_skill.id
      end

      it { should respond_with 200 }
    end

    describe 'PUT/PATCH #update' do
      context 'with valid params' do
        before do
          user_skill = create :user_skill, user: user
          put :update, id: user_skill.id, user_skill: { experience: 10 }
        end

        it 'updates the user_skill experience' do
          expect(json_response[:user_skill][:experience]).to eq 10
        end

        it { should respond_with 200 }
      end

      context 'with invalid params' do
        before do
          user_skill = create :user_skill, user: user
          put :update, id: user_skill.id, user_skill: { experience: '' }
        end

        it 'returns error cant be blank' do
          expect(json_response[:errors][:experience]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'DELETE #destroy' do
      before do
        user_skill = create :user_skill, user: user
        delete :destroy, id: user_skill.id
      end

      it { should respond_with 204 }
    end
  end
end
