require 'rails_helper'

RSpec.describe Api::V1::LocationsController, type: :controller do
  let(:user) { create :user }

  context 'when user is not logged in' do
    it 'returns error unauthorized' do
      get :index
      expect(response.response_code).to eq 401
    end
  end

  context 'when user logged in' do
    before do
      authorization_header(user.auth_token, user.email)
    end

    describe 'GET #index' do
      before do
        4.times { create :location }
        get :index
      end

      it 'renders a list of locations' do
        expect(json_response[:locations]).to have(4).items
      end

      it { should respond_with 200 }

      it_behaves_like 'paginated list'
    end

    describe 'POST #create' do
      context 'with valid params' do
        before do
          location = {
            street: 'street', number: '123', neighborhood: 'neighborhood',
            city: 'city', state: 'state', zip_code: '12345', country: 'country',
            calendar_id: create(:calendar).id
          }
          post :create, location: location
        end

        it 'renders a location record' do
          expect(json_response[:location]).to have_key(:id)
        end

        it { should respond_with 201 }
      end

      context 'with invalid params' do
        before do
          location = {
            street: '', number: '', neighborhood: '', city: '', state: '',
            zip_code: '', country: ''
          }
          post :create, location: location
        end

        it 'returns errors if location not valid' do
          expect(json_response[:errors][:street]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'GET #show' do
      before do
        @location = create :location
        get :show, id: @location.id
      end

      it 'renders a single record' do
        expect(json_response[:location][:id]).to eq @location.id
      end

      it { should respond_with 200 }
    end

    describe 'PUT/PATCH #update' do
      context 'with valid params' do
        before do
          location = create :location
          put :update, id: location.id, location: { street: 'changed' }
        end

        it 'updates the location name' do
          expect(json_response[:location][:street]).to eq 'changed'
        end

        it { should respond_with 200 }
      end

      context 'with invalid params' do
        before do
          location = create :location
          put :update, id: location.id, location: { street: '' }
        end

        it 'returns error cant be blank' do
          expect(json_response[:errors][:street]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'DELETE #destroy' do
      before do
        location = create :location
        delete :destroy, id: location.id
      end

      it { should respond_with 204 }
    end
  end
end
