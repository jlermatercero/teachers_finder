require 'rails_helper'

RSpec.describe Api::V1::PlansController, type: :controller do
  let(:user) { create :user }

  context 'when user is not logged in' do
    it 'returns error unauthorized' do
      get :index
      expect(response.response_code).to eq 401
    end
  end

  context 'when user logged in' do
    before do
      authorization_header(user.auth_token, user.email)
    end

    describe 'GET #index' do
      before do
        4.times { create :plan }
        get :index
      end

      it 'renders a list of plans' do
        expect(json_response[:plans]).to have(4).items
      end

      it { should respond_with 200 }

      it_behaves_like 'paginated list'
    end

    describe 'POST #create' do
      context 'with valid params' do
        before do
          plan = {
            name: 'my plan'
          }
          post :create, plan: plan
        end

        it 'renders a plan record' do
          expect(json_response[:plan]).to have_key(:id)
        end

        it { should respond_with 201 }
      end

      context 'with invalid params' do
        before do
          plan = {
            name: ''
          }
          post :create, plan: plan
        end

        it 'returns errors if plan not valid' do
          expect(json_response[:errors][:name]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'GET #show' do
      before do
        @plan = create :plan
        get :show, id: @plan.id
      end

      it 'renders a single record' do
        expect(json_response[:plan][:id]).to eq @plan.id
      end

      it { should respond_with 200 }
    end

    describe 'PUT/PATCH #update' do
      context 'with valid params' do
        before do
          plan = create :plan
          put :update, id: plan.id, plan: { name: 'changed' }
        end

        it 'updates the plan name' do
          expect(json_response[:plan][:name]).to eq 'changed'
        end

        it { should respond_with 200 }
      end

      context 'with invalid params' do
        before do
          plan = create :plan
          put :update, id: plan.id, plan: { name: '' }
        end

        it 'returns error cant be blank' do
          expect(json_response[:errors][:name]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'DELETE #destroy' do
      before do
        plan = create :plan
        delete :destroy, id: plan.id
      end

      it { should respond_with 204 }
    end
  end
end
