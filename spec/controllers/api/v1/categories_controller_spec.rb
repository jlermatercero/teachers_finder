require 'rails_helper'

RSpec.describe Api::V1::CategoriesController, type: :controller do
  let(:user) { create :user }

  context 'when user is not logged in' do
    it 'returns error unauthorized' do
      get :index
      expect(response.response_code).to eq 401
    end
  end

  context 'when user logged in' do
    before do
      authorization_header(user.auth_token, user.email)
    end

    describe 'GET #index' do
      before do
        4.times { create :category }
        get :index
      end

      it 'renders a list of categories' do
        expect(json_response[:categories]).to have(4).items
      end

      it { should respond_with 200 }

      it_behaves_like 'paginated list'
    end

    describe 'POST #create' do
      context 'with valid params' do
        before do
          category = {
            name: 'my category'
          }
          post :create, category: category
        end

        it 'renders a category record' do
          expect(json_response[:category]).to have_key(:id)
        end

        it { should respond_with 201 }
      end

      context 'with invalid params' do
        before do
          category = {
            name: ''
          }
          post :create, category: category
        end

        it 'returns errors if category not valid' do
          expect(json_response[:errors][:name]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'GET #show' do
      before do
        @category = create :category
        get :show, id: @category.id
      end

      it 'renders a single record' do
        expect(json_response[:category][:id]).to eq @category.id
      end

      it { should respond_with 200 }
    end

    describe 'PUT/PATCH #update' do
      context 'with valid params' do
        before do
          category = create :category
          put :update, id: category.id, category: { name: 'changed' }
        end

        it 'updates the category name' do
          expect(json_response[:category][:name]).to eq 'changed'
        end

        it { should respond_with 200 }
      end

      context 'with invalid params' do
        before do
          category = create :category
          put :update, id: category.id, category: { name: '' }
        end

        it 'returns error cant be blank' do
          expect(json_response[:errors][:name]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'DELETE #destroy' do
      before do
        category = create :category
        delete :destroy, id: category.id
      end

      it { should respond_with 204 }
    end
  end
end
