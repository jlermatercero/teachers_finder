require 'rails_helper'

RSpec.describe Api::V1::CalendarsController, type: :controller do
  let(:user) { create :user }

  context 'when user is not logged in' do
    it 'returns error unauthorized' do
      get :index
      expect(response.response_code).to eq 401
    end
  end

  context 'when user logged in' do
    before do
      authorization_header(user.auth_token, user.email)
    end

    describe 'GET #index' do
      before do
        4.times { create :calendar }
        get :index
      end

      it 'renders a list of categories' do
        expect(json_response[:calendars]).to have(4).items
      end

      it { should respond_with 200 }

      it_behaves_like 'paginated list'
    end

    describe 'POST #create' do
      context 'with valid params' do
        before do
          calendar = {
            teacher_id: create(:user).id, user_id: create(:user).id,
            date_start: FFaker::Time.date, date_end: FFaker::Time.date
          }
          post :create, calendar: calendar
        end

        it 'renders a calendar record' do
          expect(json_response[:calendar]).to have_key(:id)
        end

        it { should respond_with 201 }
      end

      context 'with invalid params' do
        before do
          calendar = {
            date_start: ''
          }
          post :create, calendar: calendar
        end

        it 'returns errors if calendar not valid' do
          expect(json_response[:errors][:date_start]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'GET #show' do
      before do
        @calendar = create :calendar
        get :show, id: @calendar.id
      end

      it 'renders a single record' do
        expect(json_response[:calendar][:id]).to eq @calendar.id
      end

      it { should respond_with 200 }
    end

    describe 'PUT/PATCH #update' do
      context 'with valid params' do
        before do
          calendar = create :calendar
          @date_start = FFaker::Time.date
          put :update, id: calendar.id, calendar: { date_start: @date_start }
        end

        it 'updates the calendar name' do
          expect(
            Time.new(json_response[:calendar][:date_start]).to_i
          ).to eq(
            Time.new(@date_start).to_i
          )
        end

        it { should respond_with 200 }
      end

      context 'with invalid params' do
        before do
          calendar = create :calendar
          put :update, id: calendar.id, calendar: { date_start: '' }
        end

        it 'returns error cant be blank' do
          expect(json_response[:errors][:date_start]).to include(
            t 'errors.messages.blank')
        end

        it { should respond_with 422 }
      end
    end

    describe 'DELETE #destroy' do
      before do
        calendar = create :calendar
        delete :destroy, id: calendar.id
      end

      it { should respond_with 204 }
    end
  end
end
