# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string
#  password_digest :string
#  auth_token      :string
#  first_name      :string
#  last_name       :string
#  avatar          :string
#  votes_count     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { create :user }

  subject { user }

  context 'attributes' do
    it { is_expected.to respond_to(:email) }
    it { is_expected.to respond_to(:auth_token) }
    it { is_expected.to respond_to(:first_name) }
    it { is_expected.to respond_to(:last_name) }
    it { is_expected.to respond_to(:avatar) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
  end

  context 'associations' do
    it { is_expected.to have_many(:user_plan_costs) }
    it { is_expected.to have_many(:votes) }
    it { is_expected.to have_many(:votes_made) }
    it { is_expected.to have_many(:user_skills) }
    it { is_expected.to have_many(:skills) }
    it { is_expected.to have_many(:calendars) }
    it { is_expected.to have_many(:teacher_calendars) }
  end

  context 'methods' do
    describe '#generate_auth_token!' do
      before(:each) do
        @user = build :user
      end

      it 'generates a unique token' do
        allow(SecureRandom).to receive(:base64).and_return('auniquetoken')
        expect { @user.generate_auth_token! }.to change {
          @user.auth_token
        }.from(nil).to('auniquetoken')
      end

      it 'avoids nil on the auth token' do
        @user.generate_auth_token!
        expect(@user.auth_token).to_not be_nil
      end
    end

    describe '#generate_avatar_md5' do
      it 'generates a MD5 hash for user avatar from email' do
        user = build(:user)
        email_hash = Digest::MD5.hexdigest(user.email)
        user.save
        user.reload
        expect(user.avatar).to eq email_hash
      end
    end
  end
end
