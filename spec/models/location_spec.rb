# == Schema Information
#
# Table name: locations
#
#  id           :integer          not null, primary key
#  street       :string
#  number       :string
#  neighborhood :string
#  city         :string
#  state        :string
#  zip_code     :integer
#  country      :string
#  lat          :string
#  lon          :string
#  calendar_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'rails_helper'

RSpec.describe Location, type: :model do
  let(:location) { create :location }

  subject { location }

  context 'attributes' do
    it { is_expected.to respond_to(:street) }
    it { is_expected.to respond_to(:number) }
    it { is_expected.to respond_to(:neighborhood) }
    it { is_expected.to respond_to(:city) }
    it { is_expected.to respond_to(:state) }
    it { is_expected.to respond_to(:zip_code) }
    it { is_expected.to respond_to(:country) }
    it { is_expected.to respond_to(:lat) }
    it { is_expected.to respond_to(:lon) }
    it { is_expected.to respond_to(:calendar_id) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:street) }
    it { is_expected.to validate_presence_of(:number) }
    it { is_expected.to validate_presence_of(:neighborhood) }
    it { is_expected.to validate_presence_of(:city) }
    it { is_expected.to validate_presence_of(:state) }
    it { is_expected.to validate_presence_of(:zip_code) }
    it { is_expected.to validate_presence_of(:country) }
    it { is_expected.to validate_presence_of(:calendar_id) }
  end

  context 'associations' do
    it { is_expected.to belong_to(:calendar) }
  end
end
