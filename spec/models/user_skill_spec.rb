# == Schema Information
#
# Table name: user_skills
#
#  id         :integer          not null, primary key
#  skill_id   :integer
#  user_id    :integer
#  experience :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe UserSkill, type: :model do
  let(:user_skill) { create :user_skill }

  subject { user_skill }

  context 'attributes' do
    it { is_expected.to respond_to(:skill_id) }
    it { is_expected.to respond_to(:user_id) }
    it { is_expected.to respond_to(:experience) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:skill_id) }
    it { is_expected.to validate_presence_of(:user_id) }
    it { is_expected.to validate_presence_of(:experience) }
  end

  context 'associations' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:skill) }
  end
end
