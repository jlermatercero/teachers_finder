# == Schema Information
#
# Table name: calendars
#
#  id         :integer          not null, primary key
#  teacher_id :integer
#  user_id    :integer
#  date_start :datetime
#  date_end   :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Calendar, type: :model do
  let(:calendar) { create :calendar }

  subject { calendar }

  context 'attributes' do
    it { is_expected.to respond_to(:teacher_id) }
    it { is_expected.to respond_to(:user_id) }
    it { is_expected.to respond_to(:date_start) }
    it { is_expected.to respond_to(:date_end) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:teacher_id) }
    it { is_expected.to validate_presence_of(:user_id) }
    it { is_expected.to validate_presence_of(:date_start) }
    it { is_expected.to validate_presence_of(:date_end) }
  end

  context 'associations' do
    it { is_expected.to have_one(:location) }
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:teacher) }
  end
end
