# == Schema Information
#
# Table name: skills
#
#  id          :integer          not null, primary key
#  name        :string
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Skill, type: :model do
  let(:skill) { create :skill }

  subject { skill }

  context 'attributes' do
    it { is_expected.to respond_to(:name) }
    it { is_expected.to respond_to(:category_id) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:category_id) }
  end

  context 'associations' do
    it { is_expected.to belong_to(:category) }
    it { is_expected.to have_many(:user_skills) }
    it { is_expected.to have_many(:users) }
  end
end
