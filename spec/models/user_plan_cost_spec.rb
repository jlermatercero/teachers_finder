# == Schema Information
#
# Table name: user_plan_costs
#
#  id         :integer          not null, primary key
#  plan_id    :integer
#  user_id    :integer
#  cost       :decimal(8, 2)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe UserPlanCost, type: :model do
  let(:user_plan_cost) { create :user_plan_cost }

  subject { user_plan_cost }

  context 'attributes' do
    it { is_expected.to respond_to(:plan_id) }
    it { is_expected.to respond_to(:user_id) }
    it { is_expected.to respond_to(:cost) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:plan_id) }
    it { is_expected.to validate_presence_of(:user_id) }
    it { is_expected.to validate_presence_of(:cost) }
  end

  context 'associations' do
    it { is_expected.to belong_to(:plan) }
    it { is_expected.to belong_to(:user) }
  end
end
