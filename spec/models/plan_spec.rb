# == Schema Information
#
# Table name: plans
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Plan, type: :model do
  let(:plan) { create :plan }

  subject { plan }

  context 'attributes' do
    it { is_expected.to respond_to(:name) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:name) }
  end

  context 'associations' do
    #  it { is_expected.to belong_to(:category) }
  end
end
