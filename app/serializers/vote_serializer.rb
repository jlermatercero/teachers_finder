#
class VoteSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :author_id
end
