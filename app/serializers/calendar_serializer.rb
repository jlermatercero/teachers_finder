#
class CalendarSerializer < ActiveModel::Serializer
  attributes :id, :teacher_id, :user_id, :date_start, :date_end
end
