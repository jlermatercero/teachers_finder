#
class LocationSerializer < ActiveModel::Serializer
  attributes :id, :street, :number, :neighborhood, :city, :state, :zip_code,
             :country, :lat, :lon, :calendar_id
end
