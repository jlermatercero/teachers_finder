#
module Api
  #
  module V1
    #
    class CalendarsController < BaseController
      before_action :authenticate!

      def index
        calendars = paginate(Calendar.all)
        render json: calendars, meta: meta_pagination(calendars), status: 200
      end

      def create
        calendar = Calendar.new(calendar_params)
        if calendar.save
          render json: calendar, status: 201
        else
          render json: { errors: calendar.errors }, status: 422
        end
      end

      def show
        calendar = Calendar.find(params[:id])
        render json: calendar, status: 200
      end

      def update
        calendar = Calendar.find(params[:id])
        if calendar.update(calendar_params)
          render json: calendar, status: 200
        else
          render json: { errors: calendar.errors }, status: 422
        end
      end

      def destroy
        Calendar.find(params[:id]).destroy
        head :no_content
      end

      private

      def calendar_params
        params.require(:calendar).permit(:teacher_id, :user_id, :date_start,
                                         :date_end)
      end
    end
  end
end
