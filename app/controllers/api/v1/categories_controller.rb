#
module Api
  #
  module V1
    #
    class CategoriesController < BaseController
      before_action :authenticate!

      def index
        categories = paginate(Category.all)
        render json: categories, meta: meta_pagination(categories), status: 200
      end

      def create
        category = Category.new(category_params)
        if category.save
          render json: category, status: 201
        else
          render json: { errors: category.errors }, status: 422
        end
      end

      def show
        category = Category.find(params[:id])
        render json: category, status: 200
      end

      def update
        category = Category.find(params[:id])
        if category.update(category_params)
          render json: category, status: 200
        else
          render json: { errors: category.errors }, status: 422
        end
      end

      def destroy
        Category.find(params[:id]).destroy
        head :no_content
      end

      private

      def category_params
        params.require(:category).permit(:name)
      end
    end
  end
end
