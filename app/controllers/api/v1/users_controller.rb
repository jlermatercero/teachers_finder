#
module Api
  #
  module V1
    #
    class UsersController < BaseController
      before_action :authenticate!

      def index
        users = paginate(User.all)
        render json: users, meta: meta_pagination(users),
               status: 200
      end

      def create
        user = User.new(user_params)
        if user.save
          render json: user, status: 201
        else
          render json: { errors: user.errors }, status: 422
        end
      end

      def show
        user = User.find(params[:id])
        render json: user, status: 200
      end

      def update
        user = User.find(params[:id])
        if user.update(user_params)
          render json: user, status: 200
        else
          render json: { errors: user.errors }, status: 422
        end
      end

      def destroy
        User.find(params[:id]).destroy
        head :no_content
      end

      private

      def user_params
        params.require(:user).permit(:email, :password, :password_confirmation,
                                     :first_name, :last_name)
      end
    end
  end
end
