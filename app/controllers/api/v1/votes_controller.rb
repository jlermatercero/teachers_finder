#
module Api
  #
  module V1
    #
    class VotesController < BaseController
      before_action :authenticate!

      def index
        votes = paginate(Vote.all)
        render json: votes, meta: meta_pagination(votes), status: 200
      end

      def create
        vote = current_user.votes_made.build(vote_params)
        if vote.save
          render json: vote, status: 201
        else
          render json: { errors: vote.errors }, status: 422
        end
      end

      def destroy
        current_user.votes_made.find(params[:id]).destroy
        head :no_content
      end

      private

      def vote_params
        params.require(:vote).permit(:user_id)
      end
    end
  end
end
