#
module Api
  #
  module V1
    #
    class LocationsController < BaseController
      before_action :authenticate!

      def index
        locations = paginate(Location.all)
        render json: locations, meta: meta_pagination(locations), status: 200
      end

      def create
        location = Location.new(location_params)
        if location.save
          render json: location, status: 201
        else
          render json: { errors: location.errors }, status: 422
        end
      end

      def show
        location = Location.find(params[:id])
        render json: location, status: 200
      end

      def update
        location = Location.find(params[:id])
        if location.update(location_params)
          render json: location, status: 200
        else
          render json: { errors: location.errors }, status: 422
        end
      end

      def destroy
        Location.find(params[:id]).destroy
        head :no_content
      end

      private

      def location_params
        params.require(:location).permit(:street, :number, :neighborhood,
                                         :city, :state, :zip_code, :country,
                                         :lat, :lan, :calendar_id)
      end
    end
  end
end
