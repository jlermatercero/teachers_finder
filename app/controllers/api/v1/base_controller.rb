#
module Api
  #
  module V1
    #
    class BaseController < ApplicationController
      protect_from_forgery with: :null_session
      attr_reader :current_user

      protected

      def authenticate!
        authenticate_with_token || render_unauthorized
      end

      def render_unauthorized
        headers['WWW-Authenticate'] = 'Token realm="Application"'
        render json: { errors: 'Invalid email or password' },
               status: :unauthorized
      end

      private

      def authenticate_with_token
        authenticate_with_http_token do |token, options|
          # Token token=2a4s5d6ft7ybiu; email=email@example.com
          user_email = options[:email]
          user = User.find_by(email: user_email) if user_email.present?

          if user && secure_token_compare(user.auth_token, token)
            @current_user = user
          end
        end
      end

      def secure_token_compare(a, b)
        return false if a.blank? || b.blank? || a.bytesize != b.bytesize
        l = a.unpack "C#{a.bytesize}"

        res = 0
        b.each_byte { |byte| res |= byte ^ l.shift }
        res == 0
      end

      def paginate(collection)
        collection.page(params[:page]).per((params[:per_page] || 10))
      end

      def meta_pagination(collection)
        {
          pagination: {
            per_page: params[:per_page] || 10,
            total_pages: collection.total_pages,
            total_objects: collection.total_count
          }
        }
      end
    end
  end
end
