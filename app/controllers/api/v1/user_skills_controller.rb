#
module Api
  #
  module V1
    #
    class UserSkillsController < BaseController
      before_action :authenticate!

      def index
        user_skills = paginate(UserSkill.all)
        render json: user_skills, meta: meta_pagination(user_skills),
               status: 200
      end

      def create
        user_skill = current_user.user_skills.build(user_skill_params)
        if user_skill.save
          render json: user_skill, status: 201
        else
          render json: { errors: user_skill.errors }, status: 422
        end
      end

      def show
        user_skill = current_user.user_skills.find(params[:id])
        render json: user_skill, status: 200
      end

      def update
        user_skill = current_user.user_skills.find(params[:id])
        if user_skill.update(user_skill_params)
          render json: user_skill, status: 200
        else
          render json: { errors: user_skill.errors }, status: 422
        end
      end

      def destroy
        current_user.user_skills.find(params[:id]).destroy
        head :no_content
      end

      private

      def user_skill_params
        params.require(:user_skill).permit(:skill_id, :experience)
      end
    end
  end
end
