#
module Api
  #
  module V1
    #
    class SkillsController < BaseController
      before_action :authenticate!

      def index
        skills = paginate(Skill.all)
        render json: skills, meta: meta_pagination(skills), status: 200
      end

      def create
        skill = Skill.new(skill_params)
        if skill.save
          render json: skill, status: 201
        else
          render json: { errors: skill.errors }, status: 422
        end
      end

      def show
        skill = Skill.find(params[:id])
        render json: skill, status: 200
      end

      def update
        skill = Skill.find(params[:id])
        if skill.update(skill_params)
          render json: skill, status: 200
        else
          render json: { errors: skill.errors }, status: 422
        end
      end

      def destroy
        Skill.find(params[:id]).destroy
        head :no_content
      end

      private

      def skill_params
        params.require(:skill).permit(:name, :category_id)
      end
    end
  end
end
