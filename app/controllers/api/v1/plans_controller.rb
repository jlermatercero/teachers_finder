#
module Api
  #
  module V1
    #
    class PlansController < BaseController
      before_action :authenticate!

      def index
        plans = paginate(Plan.all)
        render json: plans, meta: meta_pagination(plans), status: 200
      end

      def create
        plan = Plan.new(plan_params)
        if plan.save
          render json: plan, status: 201
        else
          render json: { errors: plan.errors }, status: 422
        end
      end

      def show
        plan = Plan.find(params[:id])
        render json: plan, status: 200
      end

      def update
        plan = Plan.find(params[:id])
        if plan.update(plan_params)
          render json: plan, status: 200
        else
          render json: { errors: plan.errors }, status: 422
        end
      end

      def destroy
        Plan.find(params[:id]).destroy
        head :no_content
      end

      private

      def plan_params
        params.require(:plan).permit(:name)
      end
    end
  end
end
