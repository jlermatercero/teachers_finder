#
module Api
  #
  module V1
    #
    class SessionsController < BaseController
      before_action :authenticate!, only: [:destroy]

      def create
        email = params[:session][:email]
        password = params[:session][:password]
        user = User.find_by_email(email)
        if user.present? && user.authenticate(password)
          sign_in user
          render json: { token: user.auth_token, email: user.email },
                 status: 200
        else
          render json: { errors: 'Invalid email or password' }, status: 422
        end
      end

      def destroy
        current_user.generate_auth_token!
        current_user.save
        head :no_content
      end

      private

      def sign_in(user)
        user.generate_auth_token!
        user.save
      end
    end
  end
end
