# == Schema Information
#
# Table name: locations
#
#  id           :integer          not null, primary key
#  street       :string
#  number       :string
#  neighborhood :string
#  city         :string
#  state        :string
#  zip_code     :integer
#  country      :string
#  lat          :string
#  lon          :string
#  calendar_id  :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
class Location < ActiveRecord::Base
  validates :street, :number, :neighborhood, :city, :state, :zip_code, :country,
            :calendar_id, presence: true
  belongs_to :calendar, inverse_of: :location
end
