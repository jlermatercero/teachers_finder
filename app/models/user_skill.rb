# == Schema Information
#
# Table name: user_skills
#
#  id         :integer          not null, primary key
#  skill_id   :integer
#  user_id    :integer
#  experience :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class UserSkill < ActiveRecord::Base
  validates :skill_id, :user_id, :experience, presence: true
  belongs_to :skill, inverse_of: :user_skills
  belongs_to :user, inverse_of: :user_skills
end
