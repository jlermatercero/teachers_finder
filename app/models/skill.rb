# == Schema Information
#
# Table name: skills
#
#  id          :integer          not null, primary key
#  name        :string
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Skill < ActiveRecord::Base
  validates :name, :category_id, presence: true
  belongs_to :category, inverse_of: :skills
  has_many :user_skills, inverse_of: :skill
  has_many :users, through: :user_skills, inverse_of: :skill
end
