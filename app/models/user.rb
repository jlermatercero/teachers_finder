# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  email           :string
#  password_digest :string
#  auth_token      :string
#  first_name      :string
#  last_name       :string
#  avatar          :string
#  votes_count     :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class User < ActiveRecord::Base
  has_secure_password
  validates :email, :first_name, :last_name, presence: true
  has_many :user_plan_costs, inverse_of: :user

  has_many :votes, inverse_of: :user
  has_many :votes_made, inverse_of: :author, foreign_key: 'author_id',
                        class_name: 'Vote'
  has_many :user_skills, inverse_of: :user
  has_many :skills, through: :user_skills, inverse_of: :user

  has_many :calendars, inverse_of: :user

  has_many :teacher_calendars, inverse_of: :teacher, foreign_key: 'teacher_id',
                               class_name: 'Calendar'
  before_create :generate_auth_token!
  before_save :generate_avatar_md5

  def generate_auth_token!
    loop do
      self.auth_token = SecureRandom.base64(64)
      break unless self.class.exists?(auth_token: auth_token)
    end
  end

  private

  def generate_avatar_md5
    self.avatar = Digest::MD5.hexdigest(email)
  end
end
