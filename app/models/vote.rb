# == Schema Information
#
# Table name: votes
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  author_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Vote < ActiveRecord::Base
  validates :user_id, :author_id, presence: true
  belongs_to :user, inverse_of: :votes, counter_cache: true
  belongs_to :author, inverse_of: :votes_made, class_name: 'User',
                      foreign_key: 'author_id'
end
