# == Schema Information
#
# Table name: plans
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Plan < ActiveRecord::Base
  validates :name, presence: true
  has_many :user_plan_costs, inverse_of: :plan
end
