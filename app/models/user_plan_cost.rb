# == Schema Information
#
# Table name: user_plan_costs
#
#  id         :integer          not null, primary key
#  plan_id    :integer
#  user_id    :integer
#  cost       :decimal(8, 2)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class UserPlanCost < ActiveRecord::Base
  validates :plan_id, :user_id, :cost, presence: true
  belongs_to :plan, inverse_of: :user_plan_costs
  belongs_to :user, inverse_of: :user_plan_costs
end
