# == Schema Information
#
# Table name: calendars
#
#  id         :integer          not null, primary key
#  teacher_id :integer
#  user_id    :integer
#  date_start :datetime
#  date_end   :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Calendar < ActiveRecord::Base
  validates :teacher_id, :user_id, :date_start, :date_end, presence: true
  has_one :location, inverse_of: :calendar
  belongs_to :user, inverse_of: :calendars
  belongs_to :teacher, inverse_of: :teacher_calendars, class_name: 'User',
                       foreign_key: 'teacher_id'
end
