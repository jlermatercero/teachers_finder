class CreateCalendars < ActiveRecord::Migration
  def change
    create_table :calendars do |t|
      t.integer :teacher_id
      t.integer :user_id
      t.datetime :date_start
      t.datetime :date_end

      t.timestamps null: false
    end
    add_index :calendars, :teacher_id
    add_index :calendars, :user_id
  end
end
