class CreateUserPlanCosts < ActiveRecord::Migration
  def change
    create_table :user_plan_costs do |t|
      t.integer :plan_id
      t.integer :user_id
      t.decimal :cost, precision: 8, scale: 2

      t.timestamps null: false
    end
    add_index :user_plan_costs, :plan_id
    add_index :user_plan_costs, :user_id
  end
end
