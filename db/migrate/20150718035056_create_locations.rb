class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :street
      t.string :number
      t.string :neighborhood
      t.string :city
      t.string :state
      t.integer :zip_code
      t.string :country
      t.string :lat
      t.string :lon
      t.integer :calendar_id

      t.timestamps null: false
    end
    add_index :locations, :calendar_id
  end
end
