class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.string :name
      t.integer :category_id

      t.timestamps null: false
    end
    add_index :skills, :category_id
  end
end
